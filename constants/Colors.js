const tintColor = '#87244c';

export default {
  tintColor,
  tabIconDefault: '#fff',
  tabIconSelected: '#AF2E62',
  tabBar: tintColor,
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
