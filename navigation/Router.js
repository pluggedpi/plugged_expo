import { createRouter } from '@expo/ex-navigation';

import HomeScreen from '../screens/HomeScreen';
import LoginScreen from '../screens/LoginScreen';
import UserScreen from '../screens/UserScreen';
import SettingsScreen from '../screens/SettingsScreen';
import AddEventScreen from '../screens/AddEventScreen';
import EventDetailsScreen from '../screens/EventDetailsScreen';
import RootNavigation from './RootNavigation';

export default createRouter(() => ({
  home: () => HomeScreen,
  userSc: () => UserScreen,
  addEvent: () => AddEventScreen,
  detailsEvent: () => EventDetailsScreen,
  login: () => LoginScreen,
  settings: () => SettingsScreen,
  rootNavigation: () => RootNavigation,
}));
