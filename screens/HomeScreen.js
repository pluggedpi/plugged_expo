import React from 'react';
import {
  Image,
  Linking,
  Platform,
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TouchableHighlight,
  ActivityIndicator,
} from 'react-native';
import { Components, Location, Permissions } from 'expo';

import Expo from "expo";

import { MonoText } from '../components/StyledText';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../constants/Colors';
import Router from '../navigation/Router';

const GEOLOCATION_OPTIONS = { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 };

mapStyle = [{"elementType": "geometry","stylers": [{"color": "#f5f5f5"}]},{"elementType": "geometry.stroke","stylers": [{"color": "#d70fb4"}]},{"elementType": "labels.icon","stylers": [{"color": "#d70fb4"},{"visibility": "off"}]},{"elementType": "labels.text","stylers": [{"color": "#d70fb4"}]},{"elementType": "labels.text.fill","stylers": [{"color": "#616161"}]},{"elementType": "labels.text.stroke","stylers": [{"color": "#f5f5f5"}]},{"featureType": "administrative","elementType": "labels.text.fill","stylers": [{"color": "#d70fb4"}]},{"featureType": "administrative.land_parcel","elementType": "labels.text.fill","stylers": [{"color": "#bdbdbd"}]},{"featureType": "poi","elementType": "geometry","stylers": [{"color": "#eeeeee"}]},{"featureType": "poi","elementType": "labels.text.fill","stylers": [{"color": "#757575"}]},{"featureType": "poi.park","elementType": "geometry","stylers": [{"color": "#e5e5e5"}]},{"featureType": "poi.park","elementType": "labels.text.fill","stylers": [{"color": "#9e9e9e"}]},{"featureType": "road","elementType": "geometry","stylers": [{"color": "#ffffff"}]},{"featureType": "road.arterial","elementType": "labels.text.fill","stylers": [{"color": "#757575"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"color": "#dadada"}]},{"featureType": "road.highway","elementType": "labels.text.fill","stylers": [{"color": "#616161"}]},{"featureType": "road.local","elementType": "labels.text.fill","stylers": [{"color": "#9e9e9e"}]},{"featureType": "transit.line","elementType": "geometry","stylers": [{"color": "#e5e5e5"}]},{"featureType": "transit.station","elementType": "geometry","stylers": [{"color": "#eeeeee"}]},{"featureType": "water","elementType": "geometry","stylers": [{"color": "#c9c9c9"}]},{"featureType": "water","elementType": "labels.text.fill","stylers": [{"color": "#9e9e9e"}]}
];


  const { width, height } = Dimensions.get('window');
  const ASPECT_RATIO = width / height;

// (Initial Static Location) Mumbai
  const LATITUDE = 19.0760;
  const LONGITUDE = 72.8777;

  const LATITUDE_DELTA = 0.01;
  const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


export default class HomeScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loadingEvents: true,
      logged: this.props.logged,
      markers: [],
      mapRegion: null,
      lastLat: null,
      lastLong: null,
      location: { coords: {latitude: 0, longitude: 0}},
    }
    console.log(this.props.logged);
  }



  getEventsFromApi() {
      fetch("http://anadev.com.br/plugged/api/v1/event", {
        method: "GET",
      })
    .then((response) => response.json())
    .then((responseData) => {   
      var idUserLogged = this.state.logged;   
      this.setState({
        markers: responseData.map(function(ev){
          var icon = require('../assets/icons/pin.png');
          console.log(ev.idHost);
          var hostid = ev.idHost;
          var idtransformado = hostid.slice(0, - 1);

          if (idtransformado === idUserLogged) {icon = require('../assets/icons/pin2.png');}
        return {
          id: ev.idEvent,
          icon: icon,
          title: ev.title,
          description: ev.description,
          latlng: {
            latitude: ev.locationLat,
            longitude: ev.locationLong
            }
          }
        }),
        loadingEvents: false,
      })
      
    });
    
  }

  locationChanged = (location) => {
    console.log('location');
    console.log(location);
    region = {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
      latitudeDelta: 0.00922*1.5,
      longitudeDelta: 0.00421*1.5,
    },
    this.setState({location, region})
  }

  componentDidMount() {
    console.log('entrou componentDidMount');
    this.getEventsFromApi();



    /*this.watchID = Expo.Location.watchPositionAsync((position) => {
      // Create the object to update this.state.mapRegion through the onRegionChange function
      
      console.log('entrou watchID');
      let region = {
        latitude:       position.coords.latitude,
        longitude:      position.coords.longitude,
        latitudeDelta:  0.00922*1.5,
        longitudeDelta: 0.00421*1.5
      }
      this.onRegionChange(region, region.latitude, region.longitude);
    });*/
  }

  didFocus() {
    console.log('entrou didFocus');
    setTimeout(() => {this.getEventsFromApi();}, 1000);
  }

  componentWillUnmount() {
    console.log('entrou componentWillUnmount');
    Location.watchPositionAsync(GEOLOCATION_OPTIONS, this.locationChanged);
  }

  /*onRegionChange(region, lastLat, lastLong) {
    this.setState({
      mapRegion: region,
      // If there are no new values set use the the current ones
      lastLat: lastLat || this.state.lastLat,
      lastLong: lastLong || this.state.lastLong
    });
  }*/

  /*onMapPress(e) {
    let region = {
      latitude:       e.nativeEvent.coordinate.latitude,
      longitude:      e.nativeEvent.coordinate.longitude,
      latitudeDelta:  0.00922*1.5,
      longitudeDelta: 0.00421*1.5
    }
    this.onRegionChange(region, region.latitude, region.longitude);
  }*/

  navAddEventScreen(){
    this.props.navigator.push('addEvent', {logged: this.state.logged});
  }

  clickEvent(id) {
    console.log('clicou callout: '+id);
    console.log('logge: '+this.props.logged);
    this.props.navigator.push('detailsEvent', { idEvent: id, logged: this.state.logged });
  }

  render() {
    if (this.state.loadingEvents) {
      return (
        <View style={styles.containerLoader}>
          <ActivityIndicator size='large' style={{height:80}} color='#87244c' />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>

          <Expo.MapView
          customMapStyle={mapStyle}
          style={{flex: 1}}
          /*region={this.state.mapRegion}*/
          initialRegion={{
            latitude: -26.9148724,
            longitude: -48.6622644,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          showsUserLocation={true}
          region={this.state.region}
          followUserLocation={true}
          /*onRegionChange={this.onRegionChange.bind(this)}
          onPress={this.onMapPress.bind(this)}*/

          >
             
          
              {this.state.markers.map(marker => {
                  return(
                    <Expo.MapView.Marker coordinate={marker.latlng} key={marker.id} image={marker.icon} onPress={e => console.log(e.nativeEvent)}>
                      <Expo.MapView.Callout style={styles.bubble} onPress={() => this.clickEvent(marker.id)}>
                          <View style={styles.amount}>
                            <Text style={styles.bubbleTitle}>{marker.title}   <Icon name="mail-forward" color="#87244c" style={{paddingLeft: 10,}} /></Text>
                            <Text style={styles.bubbleDesc}>{marker.description}</Text>
                          </View>
                      </Expo.MapView.Callout>
                    </Expo.MapView.Marker>
                  );
                })}
          </Expo.MapView>
          <View>
            <TouchableHighlight style={styles.addButton}
                underlayColor='#87244c' onPress={this.navAddEventScreen.bind(this)}>
                <Text style={{fontSize: 30, color: 'white'}}>+</Text>
            </TouchableHighlight>
          </View>
        </View>
      );
    }
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  containerLoader: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bubble: {
    padding: 10,
    maxWidth:250,
    maxHeight:250
  },
  bubbleTitle: {
    fontWeight:  'bold',
    color: '#87244c',
    marginBottom: 6,
  },
  bubbleDesc: {
    color: '#87244c',
    height: 100
  },
  amount: {
    flex: 1,
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 15,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 80,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 140,
    height: 38,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 23,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  addButton: {
    backgroundColor: '#87244c',
    borderColor: '#87244c',
    borderWidth: 1,
    height: 70,
    width: 70,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 20,
    right:20,
    shadowColor: "#000000",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0
    }
  },
});
