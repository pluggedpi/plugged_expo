import React from 'react';
import {
  Image,
  Linking,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  TextInput,
  TouchableHighlight,
  StatusBar,
  AsyncStorage,
  ActivityIndicator,
} from 'react-native';
import { Components } from 'expo';
import Expo from "expo";
import { Facebook } from "expo";
import { MonoText } from '../components/StyledText';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../constants/Colors';
import Router from '../navigation/Router';
import moment from 'moment';

mapStyle = [{"elementType": "geometry","stylers": [{"color": "#f5f5f5"}]},{"elementType": "geometry.stroke","stylers": [{"color": "#d70fb4"}]},{"elementType": "labels.icon","stylers": [{"color": "#d70fb4"},{"visibility": "off"}]},{"elementType": "labels.text","stylers": [{"color": "#d70fb4"}]},{"elementType": "labels.text.fill","stylers": [{"color": "#616161"}]},{"elementType": "labels.text.stroke","stylers": [{"color": "#f5f5f5"}]},{"featureType": "administrative","elementType": "labels.text.fill","stylers": [{"color": "#d70fb4"}]},{"featureType": "administrative.land_parcel","elementType": "labels.text.fill","stylers": [{"color": "#bdbdbd"}]},{"featureType": "poi","elementType": "geometry","stylers": [{"color": "#eeeeee"}]},{"featureType": "poi","elementType": "labels.text.fill","stylers": [{"color": "#757575"}]},{"featureType": "poi.park","elementType": "geometry","stylers": [{"color": "#e5e5e5"}]},{"featureType": "poi.park","elementType": "labels.text.fill","stylers": [{"color": "#9e9e9e"}]},{"featureType": "road","elementType": "geometry","stylers": [{"color": "#ffffff"}]},{"featureType": "road.arterial","elementType": "labels.text.fill","stylers": [{"color": "#757575"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"color": "#dadada"}]},{"featureType": "road.highway","elementType": "labels.text.fill","stylers": [{"color": "#616161"}]},{"featureType": "road.local","elementType": "labels.text.fill","stylers": [{"color": "#9e9e9e"}]},{"featureType": "transit.line","elementType": "geometry","stylers": [{"color": "#e5e5e5"}]},{"featureType": "transit.station","elementType": "geometry","stylers": [{"color": "#eeeeee"}]},{"featureType": "water","elementType": "geometry","stylers": [{"color": "#c9c9c9"}]},{"featureType": "water","elementType": "labels.text.fill","stylers": [{"color": "#9e9e9e"}]}
];

export default class EventDetailsScren extends React.Component {
  constructor(props) {
    super(props);    
    this.state = {
      logged: this.props.logged,
      loadingEvent: true,
      eImage: require("../assets/images/poster.png"),
      eHname: '',
      eHpicture: '',
      eIdEvent: this.props.idEvent,
      eIdHost: '',
      eTitle: '',
      elatlng: {
          latitude: 19.0760,
          longitude: 72.8777
        }, 
      eDateEvent: '',
      eDescription: '',
      eStatus: '',
      eCategories: [],
      eRequirements: [],
      eGoing: [],
      buttonLeft: <Text style={styles.toolbarButton}></Text>,
      region: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      warnCancel: <Text></Text>,
    };    
  }

  componentDidMount() {
    console.log('eventscreen logged: '+this.state.logged);
    idsocorro = this.state.logged;
    setTimeout(() => {this.getDetailsFromApi();}, 1000);
  }

  didFocus() {
    console.log('eventscreen didFocus');
    this.getDetailsFromApi();
  }

  getDetailsFromApi() {
    const detailsUrl = "http://anadev.com.br/plugged/api/v1/event/details/"+this.props.idEvent;
    
    fetch(detailsUrl, {
        method: "GET",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
      })
    .then((response) => response.json())
    .then((responseData) => { 
      console.log('infos:');
      console.log(responseData['infos']);
      hostid = responseData['infos'][0]['idHost'].slice(0, - 1);
      var evdate = responseData['infos'][0]['dateEvent'];
      evStats = responseData['infos'][0]['status'];
      var newDateF = moment(evdate).format('DD/MM/YYYY HH:mm');
      console.log('id transformado: '+hostid);
      warnCancel = <Text></Text>;
      if (evStats == 0) {
        warnCancel = <Text style={styles.warnCancel}><Icon name="exclamation-circle" color="#af3434" /> Event Cancelled!</Text>
      };
      this.setState({
        eIdHost: hostid,
        eHname: responseData['infos'][0]['name'],
        eHpicture: {uri: responseData['infos'][0]['picture']},
        eTitle: responseData['infos'][0]['title'],
        elatlng: {
          latitude: parseInt(responseData['infos'][0]['locationLat']),
          longitude: parseInt(responseData['infos'][0]['locationLong']),
        },
        eDateEvent: newDateF,
        eDescription: responseData['infos'][0]['description'],
        eImage: (responseData['infos'][0]['image']) ? {uri: responseData['infos'][0]['image']} : require("../assets/images/poster.png"),
        eStatus: responseData['infos'][0]['status'],
        eCategories: responseData['cats'],
        eRequirements: responseData['reqs'],
        eGoing: responseData['going'],
        region: {
          latitude: parseInt(responseData['infos'][0]['locationLat']),
          longitude: parseInt(responseData['infos'][0]['locationLong']),            
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        },
        loadingEvent: false,
        warnCancel: warnCancel,
      }); 
        console.log('hostid: '+hostid);
        console.log('idsocorro: '+idsocorro);
        if (hostid === idsocorro) {
          console.log('if logge: '+idsocorro);  
          if (evStats == 1) {
            buttonLeft = <Text style={styles.toolbarButton} onPress={this.disableEvent.bind(this)}>Disable</Text>;
          } else {
            buttonLeft = <Text style={styles.toolbarButton} onPress={this.enableEvent.bind(this)}>Enable</Text>;
          }
         
        } else {
          isGoing = false;
          responseData['going'].map(function(person){
            console.log('compara:');
            console.log(person.idUser);
            console.log(idsocorro);
            if (person.idUser == idsocorro) {
              isGoing = true;
            }
          });
          if (isGoing) {
            buttonLeft = <Text style={styles.toolbarButton} onPress={this.setGoing.bind(this)}>X Going</Text>
          } else {
            buttonLeft = <Text style={styles.toolbarButton} onPress={this.setGoing.bind(this)}>Going</Text>
          }                
        }
        this.setState({buttonLeft: buttonLeft});

    });
  }


  setGoing() {
    console.log('setGoing entrou');
    console.log(this.props.logged);
    console.log(this.state.eIdEvent);
    idUser = this.props.logged;
    fetch('http://anadev.com.br/plugged/api/v1/user/going', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                id: idUser,
                idEvent: this.state.eIdEvent,
              })
            })
            .then((response) => response.json())
            .then((responseData) => { 
              console.log('\nResult:\n');
              console.log(responseData);
              if (responseData.text) {
                this.setState({going: true});
                this.setState({buttonLeft: <Text style={styles.toolbarButton} onPress={this.setGoing.bind(this)}>{responseData.text}</Text>});
              } else {
                Alert.alert('Error setting going!');
              }
            });
  }

  goBack() {
    this.props.navigator.pop();
  }

  disableEvent() {
    console.log('disableEvent entrou');
    console.log(this.props.logged);
    console.log(this.state.eIdEvent);
    idUser = this.props.logged;
    fetch('http://anadev.com.br/plugged/api/v1/event/disable', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                id: idUser,
                idEvent: this.state.eIdEvent,
              })
            }).then((result) => {
              console.log('\nResult:\n');
              console.log(result);
              if (result.status == 200) {
                Alert.alert('Event disabled!');
                this.props.navigator.push('home');
              } else {
                Alert.alert('Error disabling event!');
              }
            });
  }

  enableEvent() {
    console.log('enableEvent entrou');
    console.log(this.props.logged);
    console.log(this.state.eIdEvent);
    idUser = this.props.logged;
    fetch('http://anadev.com.br/plugged/api/v1/event/enable', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                id: idUser,
                idEvent: this.state.eIdEvent,
              })
            }).then((result) => {
              console.log('\nResult:\n');
              console.log(result);
              if (result.status == 200) {
                Alert.alert('Event enabled!');
                this.props.navigator.push('home');
              } else {
                Alert.alert('Error enabling event!');
              }
            });
  }

  render() {


    if (this.state.loadingEvent) {

      return (
        <View style={styles.containerLoader}>
          <ActivityIndicator size='large' style={{height:80}} color='#87244c' />
        </View>
      );

    } else {

      console.log('lenght eCategories');
      var objC = this.state.eCategories;
      var objCL = Object.keys(objC).length;
      console.log(objCL);

      console.log('lenght reqs');
      var objR = this.state.eRequirements;
      var objRL = Object.keys(objR).length;
      console.log(objRL);

      console.log('lenght going');
      var objG = this.state.eGoing;
      var objGL = Object.keys(objG).length;
      console.log(objGL);
           
      return (
        
        <View>
          <View style={styles.toolbar}>
              <Text style={styles.toolbarButton} onPress={this.goBack.bind(this)}>Back</Text>
              <Text style={styles.toolbarTitle}>{this.state.eTitle}</Text>
              {this.state.buttonLeft}
          </View>
          <ScrollView styles={[styles.container, styles.contScrDet]}>
            <View>
              <View>
                {this.state.warnCancel}
              </View>
              <View style={styles.ewrapper}>
                <View style={[styles.econtainer]}>
                  <Image source={this.state.eImage} style={[styles.ebox,styles.eImage]}/>
                  <View style={[styles.contWrap,styles.ebox]}>
                     <Text style={styles.textTitle}>{this.state.eTitle}</Text>
                     <Text style={styles.textTopo}>{this.state.eDescription}</Text>
                     <Text style={styles.textTopo}><Icon name="clock-o" color="#87244c" /> {this.state.eDateEvent}</Text>
                     <View style={[styles.infoWrap]}>
                      <Image source={this.state.eHpicture} style={[{borderRadius:100,height:25,width:25}, styles.infoItem]}/>
                      <Text style={[styles.infoItem,{color: '#a28690'}]}>{this.state.eHname}</Text>
                     </View>
                  </View>
                </View>
              </View>
              <View style={{flex:1,marginBottom:70, marginHorizontal:10}}>

                  <Expo.MapView customMapStyle={mapStyle} style={{height:150, alignSelf: 'stretch'}} region={this.state.region}>
                    <Expo.MapView.Marker coordinate={this.state.elatlng} image={require('../assets/icons/pin.png')} />
                  </Expo.MapView>

                  <Text style={styles.textDarkTit}>Categories ({objCL})</Text>
                  <View style={styles.catWrap}>
                    {this.state.eCategories.map(function(cat, i) {
                        return(
                          <Text key={i} style={styles.catItem}>{cat.title}</Text>
                        );
                    })}
                  </View>

                  <Text style={styles.textDarkTit}>Requirements ({objRL})</Text>
                  <View style={styles.reqWrap}>
                    {this.state.eRequirements.map(function(req, i) {
                        return(
                          <Text key={i} style={styles.reqItem}>{req.title}</Text>
                        );
                    })}
                  </View>

                  <Text style={styles.textDarkTit}>Going ({objGL})</Text>
                  <View style={styles.imgWrap}>
                    {this.state.eGoing.map(function(person, i) {
                        console.log(person);
                        return(
                          <Image key={i} source={{uri: person.picture}} style={styles.imgItem} />
                        );
                    })}
                  </View>

              </View>
            </View>
          </ScrollView>  
        </View>        
      );
    }
  }
}


const styles = StyleSheet.create({
  infoWrap: {
    display: 'flex',
    flex:.5,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  infoItem: {
    marginLeft:5,
  },
  catWrap: {
    display: 'flex',
    flex:.2,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  catItem: {
    borderWidth: 1,
    borderColor: '#DC7CB8',
    padding:10,
    margin:10,
    color: '#DC7CB8',
    flex: 1,
    justifyContent: 'flex-start',
  },
  reqWrap: {
    display: 'flex',
    flex:.2,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  reqItem: {
    borderWidth: 1,
    borderColor: '#DC7CB8',
    padding:10,
    margin:10,
    color: '#DC7CB8',
    flex: 1,
    justifyContent: 'flex-start',
  },
  imgWrap: {
    display: 'flex',
    flex:.2,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  imgItem: {
    marginVertical:10,
    marginLeft:10,
    width: 30, 
    height: 30, 
    borderRadius:100,
  },
  contWrap: {
    alignItems: 'flex-start',
  },
  ewrapper: {
    flex: 1,
    marginVertical:10,
    marginHorizontal:10,
  },
  econtainer: {
    flex: .5,
    flexDirection: 'row',
    justifyContent: 'flex-start', //replace with flex-end or center
  },
  containerLoader: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  ebox: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  toolbar: {
        backgroundColor:'#87244c',
        paddingTop:30,
        paddingBottom:10,
        flexDirection:'row',
    },
    contScrDet: {
      alignItems: 'flex-start',
      justifyContent:'flex-start',
    },
  contImg: {
      flex: 1,
      flexDirection: 'row',
      paddingRight: 20,
      paddingLeft: 20,
      paddingTop: 30,
      paddingBottom: 30,
      borderBottomWidth: 1,
      borderBottomColor: '#E4E4E4'
  },
  eImage: {
    resizeMode: 'contain',
    height: 'auto',
  },
  contDet: {
      flex: 2,
      flexDirection: 'row',
  },
  textDark: {
    color: '#87244c'
  },
  textDarkTit: {
    color: '#87244c',
    fontWeight: 'bold',
    paddingTop:10,
  },
  warnCancel: {
    color: '#af3434',
    fontWeight: 'bold',
    margin:10,
    textAlign: 'center',
  },
  textTopo: {
    color: '#87244c',
    padding:10,
  },
  textTitle: {
    color: '#87244c',
    fontWeight:'bold',
    fontSize:16,
    paddingHorizontal:10,
  },
  toolbar:{
        backgroundColor:'#87244c',
        paddingTop:30,
        paddingBottom:10,
        flexDirection:'row',    //Step 1
    },
    toolbarButton:{
        width: 50,            //Step 2
        color:'#fff',
        textAlign:'center',
        marginHorizontal:5,
    },
    toolbarTitle:{
        color:'#fff',
        textAlign:'center',
        fontWeight:'bold',
        flex:1,             //Step 3
    },
  location: {
    backgroundColor: 'white',
    margin: 25,
  },
  datePicker: {
    flex: 1,
    backgroundColor: '#87244c',
    height:40,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,    
    marginVertical: 5,    
    minWidth: 50,
  },
  inputDef: {
    height: 40,
    flex: 1,
    borderWidth: 0,
    borderBottomWidth: 0,
    backgroundColor: '#87244c',
    borderRadius: 0,
    marginVertical: 5,
    minWidth: 50,
    padding: 5,
    shadowColor: 'transparent',
    shadowOpacity: 0,
    borderBottomColor: 'transparent',
  }, 
  locinput: {
    flex: 2,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    fontSize: 15,
    textAlign: 'center',
  },
  contentContainer: {
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 200,
    height: 42,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    lineHeight: 23,
    textAlign: 'center',
  },
  tabNav: {
    display: 'none',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
  },
  sendButton: {
    flex: 1,
    backgroundColor: '#87244c',
    borderColor: '#87244c',
    borderWidth: 1,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
});
