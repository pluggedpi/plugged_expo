import React from 'react';
import {
  Image,
  Linking,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  TextInput,
  TouchableHighlight,
  StatusBar,
  AsyncStorage,
} from 'react-native';
import Expo from "expo";
import { Facebook } from "expo";
//import TagInput from 'react-native-tag-input';
import TagInput from 'react-native-taginput';
//import DatePicker from 'react-native-datepicker';
import DateTimePicker from 'react-native-modal-datetime-picker';

import Icon from 'react-native-vector-icons/FontAwesome';
import Router from '../navigation/Router';

export default class AddEventScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logged: this.props.logged,
      eName:'',
      eDesc:'', 
      eLocLat:'',
      eLocLon:'',
      requirements:[],
      categories:[],
      date:'date',
      reqSuggestions:[],
      catSuggestions:[],
      isDateTimePickerVisible: false,
    };
  }

  componentDidMount() {
    this.getReqsFromApi();
    this.getCatsFromApi();
  }

  getReqsFromApi() {
      fetch("http://anadev.com.br/plugged/api/v1/event/requirements", {
        method: "GET",
      })
    .then((response) => response.json())
    .then((responseData) => { 
      console.log(responseData);     
      this.setState({
        reqSuggestions: responseData.map(function(ev){
          return JSON.stringify(ev.title)
        })
      });
      console.log(this.state.reqSuggestions);
    });
  }

  getCatsFromApi() {
      fetch("http://anadev.com.br/plugged/api/v1/event/categories", {
        method: "GET",
      })
    .then((response) => response.json())
    .then((responseData) => { 
      console.log(responseData);     
      this.setState({
        catSuggestions: responseData.map(function(ev){
          return JSON.stringify(ev.title)
        })
      });
      console.log(this.state.catSuggestions);
    });
  }

  onChangeReqs = (requirements) => {
    this.setState({
      requirements: requirements
    });
  };

  onChangeCats = (categories) => {
    this.setState({
      categories: categories,
    });
  };

   _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    console.log('A date has been picked: ', date);
    this.setState({
      date: date
    });
    this._hideDateTimePicker();
  };

  goBack() {
    this.props.navigator.pop();
  }

  sendAddEvent() {
    console.log('send entrou add event');
    console.log(this.state.eName);
    console.log('cats:');
    console.log(this.state.categories);
    console.log('reqs:');
    console.log(this.state.requirements);
    if (this.state.eName != '' && this.state.eLocLat != '' && this.state.eLocLon != '' && this.state.eDesc != '' && this.state.date != '') {
        fetch('http://anadev.com.br/plugged/api/v1/event/add', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                idHost: this.state.logged,
                title: this.state.eName,
                locationLat: this.state.eLocLat,
                locationLong: this.state.eLocLon,
                description: this.state.eDesc,
                dateEvent: this.state.date,
                status: 1,
                categories: this.state.categories,
                requirements: this.state.requirements,
              }),
            })
            .then((response) => response.json())
            .then((responseData) => { 
              console.log('idEvent:');
              console.log(responseData.idEvent);
              if (responseData.idEvent) {                
                Alert.alert('Event created successfully!');
                this.props.navigator.replace('detailsEvent', { idEvent: responseData.idEvent, logged: this.state.logged });
              } else {
                Alert.alert('Error creating event!');
              }
            });
    } else {
      Alert.alert('Information missing!');
    }
    
  }

  render() {
    const inputReqProps = {
      keyboardType: 'default',
      placeholder: 'requirements',
    };
    const today = new Date();
    const inputCatProps = {
      keyboardType: 'default',
      placeholder: 'categories',
    };
    
      return (
        
        <View>
          <View style={styles.toolbar}>
              
              <Text style={styles.toolbarButton}  onPress={this.goBack.bind(this)}>Cancel</Text>

              <Text style={styles.toolbarTitle}>Add Event</Text>
              
          </View>

          <ScrollView style={styles.contForm}>
            <TextInput
              style={styles.inputDef}
              placeholder='event name'
              onChangeText={(eName) => this.setState({eName})}
              autofocus = {true}
            />

            <TextInput
               multiline = {true}
               numberOfLines = {4}
               placeholder='description'
               onChangeText={(eDesc) => this.setState({eDesc})}
               style={styles.inputDef}
            />

            <View style={{display: 'flex', flex:.5, flexDirection: 'row', justifyContent: 'flex-start',}}>

               <TextInput
                 placeholder='latitute'
                 onChangeText={(eLocLat) => this.setState({eLocLat})}
                 style={styles.locinput}
               />

               <TextInput
                 placeholder='longitude'
                 onChangeText={(eLocLon) => this.setState({eLocLon})}
                 style={styles.locinput}
               />

            </View>
           
            <View style={{flex:1}}>
              <TouchableOpacity onPress={this._showDateTimePicker}>
                <View>
                  <TextInput style={[styles.inputDef, styles.datePicker]} id="dateinput" editable={false} defaultValue="datetime" />
                </View>
              </TouchableOpacity>
              <DateTimePicker
                isVisible={this.state.isDateTimePickerVisible}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDateTimePicker}
                mode="datetime"
              />
            </View>

            <View>

              <TagInput
                ref="tagCatInput"
                initialTags={this.state.categories}
                suggestions={this.state.catSuggestions}
                containerStyle={[styles.inputTag]}
                onChange={this.onChangeCats.bind(this)}
                placeholder="categories"
                underlineColorAndroid="transparent"
                numberOfLines={2}
                tagTextColor="white"
                tagColor="#AF2E62" />

                <TagInput
                ref="tagReqInput" 
                initialTags={this.state.requirements} 
                suggestions={this.state.reqSuggestions}
                containerStyle={[styles.inputTag]} 
                onChange={this.onChangeReqs.bind(this)} 
                placeholder="requirements"
                underlineColorAndroid="transparent"
                numberOfLines={2}
                tagTextColor="white"
                tagColor="#AF2E62" />

            </View>


            <View>
              <TouchableHighlight style={styles.sendButton}
                  underlayColor='#E2C0DC' onPress={this.sendAddEvent.bind(this)}>
                  <Text style={{fontSize: 14, color: 'white', fontWeight: 'bold'}}>Send</Text>
              </TouchableHighlight>
            </View>

          </ScrollView>
        </View>
      );

  }
}

const styles = StyleSheet.create({
  toolbar:{
        backgroundColor:'#87244c',
        paddingTop:30,
        paddingBottom:10,
        flexDirection:'row'    //Step 1
    },
    toolbarButton:{
        width: 50,            //Step 2
        color:'#fff',
        textAlign:'center'
    },
    toolbarTitle:{
        color:'#fff',
        textAlign:'center',
        fontWeight:'bold',
        flex:1                //Step 3
    },
  location: {
    backgroundColor: 'white',
    margin: 25
  },
  datePicker: {
    flex: 1,
    backgroundColor: '#87244c',
    height:40,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,    
    marginVertical: 5,    
    minWidth: 50,
  },
  inputDef: {
    height: 40,
    flex: 1,
    borderWidth: 0,
    borderBottomWidth: 0,
    backgroundColor: '#87244c',
    borderRadius: 0,
    marginVertical: 5,
    minWidth: 50,
    padding: 5,
    shadowColor: 'transparent',
    shadowOpacity: 0,
    borderBottomColor: 'transparent',
    color: '#fff'
  }, 
  inputTag: {
    height: 60,
    height: 40,
    flex: 1,
    borderWidth: 0,
    borderBottomWidth: 0,
    backgroundColor: '#87244c',
    borderRadius: 0,
    marginVertical: 5,
    minWidth: 50,
    padding: 5,
    shadowColor: 'transparent',
    shadowOpacity: 0,
    borderBottomColor: 'transparent',
  },
  locinput: {
    flex: 1,
    justifyContent: 'flex-start',
    padding:10,
    height: 40,
    borderWidth: 0,
    borderBottomWidth: 0,
    backgroundColor: '#87244c',
    borderRadius: 0,
    marginVertical: 5,
    padding: 5,
    shadowColor: 'transparent',
    shadowOpacity: 0,
    borderBottomColor: 'transparent',
    color: '#fff'
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginHorizontal: 10,
  },
  contForm: {
    margin: 10,
  },
  developmentModeText: {
    marginBottom: 20,
    fontSize: 15,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 0,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 200,
    height: 42,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    lineHeight: 23,
    textAlign: 'center',
  },
  tabNav: {
    display: 'none',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
  },
  sendButton: {
    flex: 1,
    backgroundColor: '#87244c',
    borderColor: '#87244c',
    borderWidth: 1,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
});
