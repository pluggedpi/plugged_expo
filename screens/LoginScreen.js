import React from 'react';
import {
  Image,
  Linking,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  AsyncStorage,
} from 'react-native';
import {
  StackNavigation
} from '@expo/ex-navigation';
import Expo from "expo";
import { Facebook } from "expo";
import Router from '../navigation/Router';

export default class LoginScreen extends React.Component {

  componentDidMount() {
    this._loadInitialState().done();
  }

  async _loadInitialState() {
    try {
      var value = await AsyncStorage.getItem('user');
      if (value !== null){
        this.setState({logged: value});
        console.log('Recovered selection from disk: ' + value);
      } else {
        console.log('Initialized with no selection on disk.');
      }
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      logged: false,
    };
  }

  async logIn() {
    const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync('260258887778258', {
        permissions: ['public_profile', 'user_birthday'],
      });
    console.log(type);
    if (type === 'success') {

      try {
        // Get the user's name using Facebook's Graph API
        const data = await fetch(
            `https://graph.facebook.com/me?access_token=${token}&fields=id,name,picture,birthday`)
        .then((response) => response.json())
        .then((responseData) => {
           console.log("inside responsejson");
           console.log('response object:',responseData)
           console.log('token:',token);
           AsyncStorage.setItem('key', token);
           AsyncStorage.setItem('user', responseData.id);
           this.setState({logged: responseData.id});
           // send data from facebook response to my api
           
           fetch('http://anadev.com.br/plugged/api/v1/user/login', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                id: responseData.id,
                name: responseData.name,
                picture: responseData.picture.data.url,
                birth: responseData.birthday,
                token: token,
              })
            }).then((result) => {
              console.log('\nResult:\n');
              console.log(result);
              this.props.navigator.push('home', { logged: this.state.logged });
            });

         }).done(); 
            
        
          
      } catch(error) { console.error(error); }
    }
  }
  static route = {
    navigationBar: {
      visible: false,
    },
    showTabBar: false,
  };

  
  render() {
  
    if (this.state.logged){
      console.log('state logged true');
      setTimeout(() => {
        this.props.navigator.push('home', { logged: this.state.logged });
      }, 50);
      return null;
    } else {
      console.log('state logged false');
      return (
        <View style={styles.container}>
          <ScrollView
            style={styles.container}
            contentContainerStyle={styles.contentContainer}
          >

            <View style={styles.welcomeContainer}>
              <Image
                source={require("../assets/images/wordmark.png")}
                style={styles.welcomeImage}
              />
            </View>

            <View style={styles.helpContainer}>
              <TouchableOpacity onPress={() => this.logIn()} style={styles.helpLink}>
                <Text style={styles.helpLinkText}>
                  Login with Facebook
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 15,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 80,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 200,
    height: 42,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 23,
    textAlign: 'center',
  },
  tabNav: {
    display: 'none',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  tabNav: {
    height: 0,
  },
});
