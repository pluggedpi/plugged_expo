import React from 'react';
import {
  Image,
  Linking,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  TextInput,
  TouchableHighlight,
  StatusBar,
  AsyncStorage,
  ActivityIndicator
} from 'react-native';
import Expo from "expo";
import { Facebook } from "expo";
import Router from '../navigation/Router';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class UserScreen extends React.Component {

  componentWillMount() {
    console.log('entrou user');
    this._loadInitialState().done();
  }

  async _loadInitialState() {
    try {
      var value = await AsyncStorage.getItem('user');
      if (value !== null){
        this.setState({logged: value});
        console.log('Recovered selection from disk: ' + value);
        setTimeout(() => {this.getDetailsFromApi().done();}, 1000);
      } else {
        console.log('Initialized with no selection on disk.');
      }
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      logged: false,
      hosted: [],
      going: [],
      user: {},
      loadingEvents: true,
    };
  }

  async getDetailsFromApi() {

      console.log('user:');
      console.log(this.state.logged);
    const detailsUrl = "http://anadev.com.br/plugged/api/v1/user/"+this.state.logged+"/events";
      console.log('getDetailsFromApi');
    fetch(detailsUrl, {
        method: "GET",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
      })
    .then((response) => response.json())
    .then((responseData) => { 
      console.log('resposta:');
      console.log(responseData);
      this.setState({
        user: {
          name: responseData['user'][0]['name'],
          picture: responseData['user'][0]['picture'],
        },
        hosted: responseData['hosted'].map(function(ev){
        return {
          id: ev.idEvent,
          title: ev.title,
          description: ev.description,
          }
        }),
        going: responseData['going'].map(function(ev){
        return {
          id: ev.idEvent,
          title: ev.title,
          description: ev.description,
          }
        }),
        loadingEvents: false,
      })
    });
  }

  clickEvent(id) {
    console.log('clicou event: '+id);
    console.log('state logged: '+this.props.logged);
    this.props.navigator.push('detailsEvent', { idEvent: id, logged: this.state.logged });
  }

  logout() {
    AsyncStorage.removeItem('key');
    AsyncStorage.removeItem('user');
    this.props.navigator.push('login', { logged: false });
  }

  goBack() {
    this.props.navigator.pop();
  }

  render() {


    if (this.state.loadingEvents) {
      return (
        <View style={styles.containerLoader}>
          <ActivityIndicator size='large' style={{height:80}} color='#87244c' />
        </View>
      );
    } else {
      console.log('lenght hosted');
      var objH = this.state.hosted;
      var objHL = Object.keys(objH).length;
      console.log(objHL);

      console.log('lenght going');
      var objG = this.state.going;
      var objGL = Object.keys(objG).length;
      console.log(objGL);

      return (
        
        <View>

          <View style={styles.toolbar}>
              <Text style={styles.toolbarButton} onPress={this.goBack.bind(this)}>Back</Text>
              <Text style={styles.toolbarTitle}>My Events</Text>
              <Text style={styles.toolbarButton} onPress={this.logout.bind(this)}>Logout</Text>
          </View>
          <ScrollView styles={[styles.container, styles.contScrDet]}>
            <View style={styles.infoWrap}>
              <Image source={{uri: this.state.user.picture}} style={[{height:80,width:80,borderRadius:100},styles.infoItem]} />
              <Text style={[styles.textDark, styles.infoItem]}>{this.state.user.name}</Text>
            </View>
            <View style={{flex:1,marginBottom:10, marginHorizontal:10}}>
              <Text style={styles.textDarkTit}>Hosting ({objHL})</Text>
              {this.state.hosted.map(ev => {
                  return(
                    <View key={ev.id} style={styles.contDet}>
                      <Text style={styles.textDark} onPress={() => this.clickEvent(ev.id)}><Icon name="map-pin" color="#87244c" /> {ev.title}</Text>
                    </View>
                  );
                })}
            </View>
            <View style={{flex:1,marginBottom:70, marginHorizontal:10}}>
              <Text style={styles.textDarkTit}>Going ({objGL})</Text>
              {this.state.going.map(ev => {
                return(
                  <View key={ev.id} style={styles.contDet}>
                    <Text style={styles.textDark} onPress={() => this.clickEvent(ev.id)}><Icon name="map-pin" color="#87244c" /> {ev.title}</Text>
                  </View>
                );
                })}
            </View>
          </ScrollView>  
        </View>        
      );
    }
  }
}

const styles = StyleSheet.create({
  infoWrap: {
    display: 'flex',
    flex:.5,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  infoItem: {
    margin:10,
  },
  toolbar: {
        backgroundColor:'#87244c',
        paddingTop:30,
        paddingBottom:10,
        flexDirection:'row',
    },
  containerLoader: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
    contScrDet: {
      alignItems: 'center',
      justifyContent:'center',
    },
  contImg: {
      flex: 1,
      flexDirection: 'row',
      paddingRight: 20,
      paddingLeft: 20,
      paddingTop: 30,
      paddingBottom: 30,
      borderBottomWidth: 1,
      borderBottomColor: '#E4E4E4'
  },
  contDet: {
      flex: 2,
      flexDirection: 'row',
      padding: 10,
  },
  textDark: {
    color: '#87244c'
  },
  textDarkTit: {
    color: '#87244c',
    fontWeight: 'bold',
    paddingTop:10,
  },
  toolbar:{
        backgroundColor:'#87244c',
        paddingTop:30,
        paddingBottom:10,
        flexDirection:'row',    //Step 1
    },
    toolbarButton:{
        width: 50,            //Step 2
        color:'#fff',
        textAlign:'center',
    },
    toolbarTitle:{
        color:'#fff',
        textAlign:'center',
        fontWeight:'bold',
        flex:1,             //Step 3
    },
  location: {
    backgroundColor: 'white',
    margin: 25,
  },
  datePicker: {
    flex: 1,
    backgroundColor: '#87244c',
    height:40,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,    
    marginVertical: 5,    
    minWidth: 50,
  },
  inputDef: {
    height: 40,
    flex: 1,
    borderWidth: 0,
    borderBottomWidth: 0,
    backgroundColor: '#87244c',
    borderRadius: 0,
    marginVertical: 5,
    minWidth: 50,
    padding: 5,
    shadowColor: 'transparent',
    shadowOpacity: 0,
    borderBottomColor: 'transparent',
  }, 
  locinput: {
    flex: 2,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginHorizontal: 10,
  },
  developmentModeText: {
    marginBottom: 20,
    fontSize: 15,
    textAlign: 'center',
  },
  contentContainer: {
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 200,
    height: 42,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    lineHeight: 23,
    textAlign: 'center',
  },
  tabNav: {
    display: 'none',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
  },
  sendButton: {
    flex: 1,
    backgroundColor: '#87244c',
    borderColor: '#87244c',
    borderWidth: 1,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
});
